package com.mastip.cumlaude.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mastip.cumlaude.Adapter.ViewHolder.CatatanViewHolder;
import com.mastip.cumlaude.Model.CatatanModel;
import com.mastip.cumlaude.R;

import java.util.ArrayList;

/**
 * Created by HateLogcatError on 9/3/2017.
 */

public class AdapterListCatatan extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context context;
    ArrayList<CatatanModel> data;

    public AdapterListCatatan(Context context, ArrayList<CatatanModel> data) {
        this.context = context;
        this.data = data;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.adapter_catatan_layout, null);

        return new CatatanViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(holder instanceof CatatanViewHolder) {
            CatatanModel catatanModel = data.get(position);

            ((CatatanViewHolder) holder).setupUI(catatanModel);
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }
}
